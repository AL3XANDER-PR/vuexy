export default [
  {
    title: 'Dashboards',
    icon: 'HomeIcon',
    tag: 'new',
    tagVariant: 'light-success',
    children: [
      {
        title: 'modules',
        route: 'dashboard-modules',
      },
    ],
  },
]
