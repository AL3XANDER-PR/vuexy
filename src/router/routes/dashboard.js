export default [
  // {
  //   path: '/dashboard/analytics',
  //   name: 'dashboard-analytics',
  //   component: () => import('@/views/dashboard/analytics/Analytics.vue'),
  // },
  {
    path: '/dashboard/general',
    name: 'generalMaestro',
    component: () => import('@/views/dashboard/ecommerce/Ecommerce.vue'),
  },
  {
    path: '/modules',
    name: 'dashboard-modules',
    component: () => import('@/views/dashboard/modules/Modules.vue'),
  },
  {
    path: '/modules/:id',
    name: 'id-dashboard-modules',
    component: () => import('@/views/dashboard/modules/Modules.vue'),
    props: route => {
      const id = Number(route.params.id)
      return Number.isNaN(id) ? { id: 1 } : { id }
    },
  },
  // {
  //   path: '/modules/rrhh/:id',
  //   name: 'recursos-humanos',
  //   component: () => import('@/views/dashboard/modules/RecursosHumanos.vue'),
  //   props: route => {
  //     const id = Number(route.params.id)
  //     return Number.isNaN(id) ? { id: 1 } : { id }
  //   },
  // },
  {
    path: '/modules/proveedor',
    name: 'personas',
    component: () => import('@/modules/Personas/Layouts/PersonasLayout.vue'),
    children: [
      {
        path: '',
        name: 'proveedorPrincipal',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/ListaPersonas.vue'),
        meta: {
          pageTitle: 'Lista de Proveedores',
          breadcrumb: [
            {
              text: 'Proveedores',
              active: true,
            },
          ],
        },
      },
      {
        path: '/edit/:id',
        name: 'editar-usuario',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/UsersEdit.vue'),
        props: route => {
          const id = Number(route.params.id)
          return Number.isNaN(id) ? { id: 1 } : { id }
        },
        meta: {
          pageTitle: 'Editar',
          breadcrumb: [
            {
              text: 'Personas',
            },
            {
              text: 'Editar',
              active: true,
            },
          ],
        },

      },
      {
        path: '/detalles/:id',
        name: 'detalles-usuario',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/UsersView.vue'),
        props: route => {
          const id = Number(route.params.id)
          return Number.isNaN(id) ? { id: 1 } : { id }
        },
        meta: {
          pageTitle: 'Detalles',
          breadcrumb: [
            {
              text: 'Personas',
            },
            {
              text: 'Detalles',
              active: true,
            },
          ],
        },

      },
    ],
  },
  {
    path: '/modules/Clientes/',
    name: 'personas',
    component: () => import('@/modules/Personas/Layouts/PersonasLayout.vue'),
    children: [
      {
        path: '',
        name: 'clientePrincipal',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/ListaPersonas.vue'),
        meta: {
          pageTitle: 'Lista de Clientes',
          breadcrumb: [
            {
              text: 'Cliente',
              active: true,
            },
          ],
        },
      },
      {
        path: '/edit/:id',
        name: 'editar-usuario',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/UsersEdit.vue'),
        props: route => {
          const id = Number(route.params.id)
          return Number.isNaN(id) ? { id: 1 } : { id }
        },
        meta: {
          pageTitle: 'Editar',
          breadcrumb: [
            {
              text: 'Personas',
            },
            {
              text: 'Editar',
              active: true,
            },
          ],
        },
      },
      {
        path: 'add',
        name: 'add-usuario',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/AddUserView.vue'),
        meta: {
          pageTitle: 'Add',
          breadcrumb: [
            {
              text: 'Personas',
            },
            {
              text: 'Editar',
              active: true,
            },
          ],
        },

      },
      {
        path: '/detalles/:id',
        name: 'detalles-usuario',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/UsersView.vue'),
        props: route => {
          const id = Number(route.params.id)
          return Number.isNaN(id) ? { id: 1 } : { id }
        },
        meta: {
          pageTitle: 'Detalles',
          breadcrumb: [
            {
              text: 'Personas',
            },
            {
              text: 'Detalles',
              active: true,
            },
          ],
        },

      },
    ],
  },
  {
    path: '/modules/almacen',
    name: 'personas',
    component: () => import('@/modules/Personas/Layouts/PersonasLayout.vue'),
    children: [
      {
        path: '',
        name: 'almacenPrincipal',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/ListaPersonas.vue'),
        meta: {
          pageTitle: 'Lista de Almacen',
          breadcrumb: [
            {
              text: 'Almacen',
              active: true,
            },
          ],
        },
      },
      // {
      //   path: 'pruebas-personas',
      //   name: 'pruebas-personas',
      //   component: () => import(/* webpackChunkName: "Login" */ '@/modules/personas/views/Pruebas.vue'),
      //   meta: {
      //     pageTitle: 'Pruebas',
      //     breadcrumb: [
      //       {
      //         text: 'Personas',
      //         active: true,
      //       },
      //     ],
      //   },
      // },
      // {
      //   path: 'modulos-template',
      //   name: 'modulos-template',
      //   component: () => import(/* webpackChunkName: "Login" */ '@/modules/personas/views/Modulos.vue'),
      //   meta: {
      //     layout: 'full',
      //   },
      //   /* meta: {
      //     pageTitle: 'Pruebas',
      //     breadcrumb: [
      //       {
      //         text: 'Personas',
      //         active: true,
      //       },
      //     ],
      //   }, */
      // },
      {
        path: '/edit/:id',
        name: 'editar-usuario',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/UsersEdit.vue'),
        props: route => {
          const id = Number(route.params.id)
          return Number.isNaN(id) ? { id: 1 } : { id }
        },
        meta: {
          pageTitle: 'Editar',
          breadcrumb: [
            {
              text: 'Personas',
            },
            {
              text: 'Editar',
              active: true,
            },
          ],
        },

      },
      {
        path: '/detalles/:id',
        name: 'detalles-usuario',
        component: () => import(/* webpackChunkName: "Login" */ '@/modules/Personas/views/UsersView.vue'),
        props: route => {
          const id = Number(route.params.id)
          return Number.isNaN(id) ? { id: 1 } : { id }
        },
        meta: {
          pageTitle: 'Detalles',
          breadcrumb: [
            {
              text: 'Personas',
            },
            {
              text: 'Detalles',
              active: true,
            },
          ],
        },

      },
    ],
  },
]
