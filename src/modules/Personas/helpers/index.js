import axios from '@axios'

// eslint-disable-next-line import/prefer-default-export

export const avatarText = value => {
  if (!value) return ''
  const nameArray = value.split(' ')
  const arr = nameArray.map(word => word.charAt(0).toUpperCase())
  if (arr.length > 1) return arr[0] + arr[1]
  return ''
}

export const cbxGeneralDetalle = async (codGen) => {
  try {
    const resp = await axios({
      method: 'GET',
      url: `https://apigateway.adcapricornio.com:4443/v1/lvlexport/utilitarios/${codGen}?page=1&perPage=1000&desc&nomb`,
      headers: {
        Authorization: process.env.VUE_APP_TOKEN,
      },
    }).then(res => {
      const sa = res.data.map(item => ({
        value: item.value,
        text: item.text,
      }))
      sa.unshift({
        value: 0,
        text: 'SELECCIONE',
      })
      return sa
    })
    return resp
    // eslint-disable-next-line prefer-destructuring
  } catch (error) {
    return error.response.data
  }
}
