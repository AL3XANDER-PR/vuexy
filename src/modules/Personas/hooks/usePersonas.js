import { ref, watch, computed } from '@vue/composition-api'
import store from '@/store'

// import { useToast } from 'vue-toastification/composition'
// import ToastificationContent from '@core/components/toastification/ToastificationContent.vue'

const usePersonas = () => {
  const userData = ref(null)

  const getUser = id => {
    // setTimeout(() => {
    store.dispatch('PersonasStore/getUser', { id })
      .then(response => {
        userData.value = response
      })
      .catch(error => {
        if (error.response.status === 404) {
          userData.value = undefined
        }
      })
    // }, 3000)
  }

  return {
    getUser,
    userData,
  }
}

export default usePersonas
