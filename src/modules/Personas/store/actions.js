/* export const myAccion = async ({commit}) => {

} */

import axios from '@axios'

// eslint-disable-next-line import/prefer-default-export
export const fetchUsers = async (ctx, queryParams) => {
  try {
    const { data } = await axios({
      method: 'GET',
      url: 'https://apigateway.adcapricornio.com:4443/v1/lvlexport/clientes/',
      headers: {
        Authorization: process.env.VUE_APP_TOKEN,
      },
      params: queryParams,
    })
    return [
      200,
      {
        users: data.data,
        total: data.total,
        colModel: data.colModel,
      },
    ]
  } catch (error) {
    return error.response.data
  }
}

export const getUser = async (ctx, { id }) => {
  console.log(id)
  try {
    const { data } = await axios({
      method: 'GET',
      url: `${process.env.VUE_APP_API}users/${id}`,
      headers: {
        Authorization: process.env.VUE_APP_TOKEN,
      },
    })
    return data
  } catch (error) {
    return error.response.data
  }
}

export const addUser = async (ctx, userData) => {
  try {
    const { data } = await axios({
      method: 'POST',
      url: `${process.env.VUE_APP_API}users`,
      data: userData,
    })
    return data
  } catch (error) {
    return error.response.data
  }
}

export const updateUser = async (ctx, {
  id, estado, image, name, username,
}) => {
  console.log(estado, image, name, username)
  try {
    const { data } = await axios({
      method: 'PUT',
      url: `${process.env.VUE_APP_API}users/${id}`,
      data: {
        estado,
        image,
        name,
        username,
      },
      headers: {
        Authorization: process.env.VUE_APP_TOKEN,
      },
    })
    return data
  } catch (error) {
    return error.response.data
  }
}

export const deleteUser = async (ctx, id) => {
  try {
    const { data } = await axios({
      method: 'DELETE',
      url: `${process.env.VUE_APP_API}users/${id}`,
      data: { id },
    })
    return data
  } catch (error) {
    return error.response.data
  }
}

export const uploadImage = async (ctx, files) => {
  const formData = new FormData()
  // eslint-disable-next-line no-restricted-syntax
  for (const file of files) {
    formData.append('files', file)
  }

  try {
    const data = await axios({
      method: 'POST',
      url: `${process.env.VUE_APP_API}users/upload`,
      data: formData,
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: process.env.VUE_APP_TOKEN,
      },
    })
    return data
  } catch (error) {
    return error.response.data
  }
}
