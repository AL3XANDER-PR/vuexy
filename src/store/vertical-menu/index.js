import { $themeConfig } from '@themeConfig'

export default {
  namespaced: true,
  state: {
    isVerticalMenuCollapsed: $themeConfig.layout.menu.isCollapsed,
    itemMenu: [],
  },
  getters: {
    getItemsMenu(state) {
      return state
    },
  },
  mutations: {
    UPDATE_VERTICAL_MENU_COLLAPSED(state, val) {
      state.isVerticalMenuCollapsed = val
    },
    SET_ITEM_MENU(state, val) {
      state.itemMenu = val
    },
  },
  actions: {},
}
